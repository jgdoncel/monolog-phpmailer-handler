PhpMailerHandler
===============


Monolog handler to send notifications using PHP Mailer.

Requirements
------------

- PHP 5.4 or above
- PHP Mailer 6.0 or above

Instalation with composer
-------------------------

1. Open your project directory;
2. Run `composer require jgdoncel/monolog-handler-phpmailer` to add `PhpMailerHandler` in your project vendor.

Declaring handler object
------------------------

TODO

**Example:**

TODO
