<?php

namespace Ayco\Monolog\Handler;

use PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as PHPMailerException;
use Monolog\Logger;
use Monolog\Handler\MailHandler;

/**
 * Uses PHP Mailer to send the emails
 *
 * @author Jose García <Ayco@gmail.com>
 */
class PhpMailerHandler extends MailHandler
{
    private $to;
    private $conf;

    public function __construct(array $to, array $conf, $bubble = true, $level = Logger::DEBUG)
    {
        parent::__construct($level, $bubble);
        if (empty($to) || empty($conf)) {
            throw new \InvalidArgumentException('PHPMailerHandler params error');
        }
        $this->to = $to;
        $this->conf = $conf;
    }

    protected function send($content, array $records)
    {
        $mailer = new PHPMailer\PHPMailer(true);
        $mailer->ClearAllRecipients();
        $mailer->IsSMTP();
        $mailer->IsHTML(true);
        $mailer->SMTPAuth = isset($this->conf['smtpauth']) ? $this->conf['smtpauth'] : false;
        $mailer->CharSet = isset($this->conf['charset']) ? $this->conf['charset'] : 'UTF-8';
        $mailer->Port = isset($this->conf['port']) ? $this->conf['port'] : 25;
        $mailer->Host = $this->conf['host'];
        $mailer->Username = $this->conf['username'];
        $mailer->Password = $this->conf['password'];
        $mailer->From = $this->conf['from'];
        $mailer->FromName = $this->conf['fromname'];
        $mailer->Subject = $this->conf['subject'];
        $mailer->Body = $content;
        foreach ($this->to as $v) $mailer->AddAddress($v);
        if (isset($this->conf['replytoaddress']) && isset($this->conf['replytoname'])) {
            $mailer->AddReplyTo($this->conf['replytoaddress'], $this->conf['replytoname']);
        }
        if (isset($this->conf['debug'])) {
            $mailer->SMTPDebug = $this->conf['debug'];
        }
        try {
            $mailer->send();
        } catch (PHPMailerException $e) {
            throw new \InvalidArgumentException('PHPMailerHandler send failed');
        }
    }
}
