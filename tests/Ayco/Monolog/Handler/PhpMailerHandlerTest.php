<?php

namespace Ayco\Monolog\Handler;

use Monolog\Logger;

/**
 * @author Rafael Mello <merorafael@gmail.com>
 *
 * @see    https://core.telegram.org/bots/api
 */
class PhpMailerHandlerTest extends TestCase
{

    /**
     * Just check if the CacheTest has no syntax error
     */
    public function testIsThereAnySyntaxError(){
        $var = new Ayco\Monolog\Handler\PhpMailerHandler();
        $this->assertTrue(is_object($var));
        unset($var);
    }

}
